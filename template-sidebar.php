<?php
/**
 * Template name: Page with sidebar 
 */
?>
<?php get_header(); ?>  
<?php
if ( ! is_active_sidebar( 'blog-sidebar' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area">
	<?php dynamic_sidebar( 'blog-sidebar' ); ?>
</aside><!-- #secondary -->

<?php get_footer();?>