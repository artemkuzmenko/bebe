<?php
/**
 * Template name: Contacts template
 */
?>
<?php get_header(); ?> 
<?php while ( have_posts() ) : the_post() ?>
 <!-- Contacts -->
 <article class="contacts">

<div class="info-line cf">
    <div class="map">
        <?php 

        $adress = get_post_meta(get_the_ID(), 'bebe_adress', true);
        $api_key_google_map = get_post_meta(get_the_ID(), 'bebe_google_maps_api', true);

        echo do_shortcode('[pw_map address="'.esc_attr($adress).'" width="490px" height="340px" key="'.esc_attr($api_key_google_map).'"]'); ?>        
    </div>
    <?php the_content(); ?>
    <div class="contactos">
        <?php if((get_post_meta(get_the_ID(), 'bebe_adress', true))) { ?>
        <div class="adress">
            <div class="icon"></div>
            <h3><?php echo esc_attr(get_post_meta(get_the_ID(), 'bebe_adress_label', true)); ?></h3>
            <p><?php echo esc_attr(get_post_meta(get_the_ID(), 'bebe_adress', true)); ?></p>
        </div>
        <?php } if((get_post_meta(get_the_ID(), 'bebe_phone', true))) {?>
        <div class="phone">
            <div class="icon"></div>
            <h3><?php echo esc_attr(get_post_meta(get_the_ID(), 'bebe_phone_label', true)); ?></h3>
            <p><?php echo esc_attr(get_post_meta(get_the_ID(), 'bebe_phone', true)); ?></p>
        </div>
        <?php } if((get_post_meta(get_the_ID(), 'bebe_email', true))) { ?>
        <div class="email">
            <div class="icon"></div>
            <h3><?php echo esc_attr(get_post_meta(get_the_ID(), 'bebe_email_label', true)); ?></h3>
            <p><?php echo esc_attr(get_post_meta(get_the_ID(), 'bebe_email', true)); ?></p>
        </div>
        <?php } ?>
    </div>
</div>

<!-- -->

<div class="respond">
    <div class="top"> <h2>Get in touch with us</h2> </div>
    <form class="cf" method="post" id="respond-form">
    <?php echo do_shortcode(get_post_meta(get_the_ID(), 'bebe_contacts_form_shortcode', true)); ?>
    </form>
</div>

</article>
<?php endwhile; ?>

<?php get_footer();?>